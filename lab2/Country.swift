//
//  Country.swift
//  lab2
//
//  Created by Дмитрий Мишин on 21.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class Country {
    public var name: String?
    public var code: String?
    public var isoNumber: String?
    
    init(name: String, code: String, isoNumber: String) {
        self.name = name
        self.code = code
        self.isoNumber = isoNumber
    }
    
    init(dictinary: [String: Any]) {
        self.name = dictinary["countryname"] as? String
        self.code = dictinary["iso2"] as? String
        self.isoNumber = dictinary["iso3number"] as? String
    }
    
    static func decodeProductsArray(obj: Any) -> [Country] {
        let epsDictinaries = obj as! [[String: Any]]
        
        var countries = [Country]()
        
        for item in epsDictinaries {
            let country = Country(dictinary: item)
            
            countries.append(country)
        }
        
        return(countries)
    }
}

