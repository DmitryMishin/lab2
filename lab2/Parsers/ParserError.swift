//
//  ParserError.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

enum ParserError: Error {
    case ParseError
    case NotImplementsMethod
}
