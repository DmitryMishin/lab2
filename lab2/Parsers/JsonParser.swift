//
//  JsonParser.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class JsonParser<T> {
    
    private let decoder: DataHandler
    
    typealias DataHandler = ((Any) -> T)
    
    init(decoder: @escaping DataHandler) {
        self.decoder = decoder;
    }
    
    func serialize(obj: Any) throws -> String {
        throw ParserError.NotImplementsMethod
    }
    
    private func _parse(_ jsonData: Data?) throws -> Any
    {
        if let data = jsonData {
            do {
                let jsonDictinary = try JSONSerialization
                    .jsonObject(with: data, options: JSONSerialization.ReadingOptions
                        .mutableContainers)
                
                return(jsonDictinary)
            } catch {
                throw ParserError.ParseError
            }
        }
        
        throw ParserError.ParseError
    }
    
    func deserialize(path: String, type: String) throws -> T? {
        let jsonFile = Bundle.main.path(forResource: path, ofType: type)
        let jsonFileUrl = URL(fileURLWithPath: jsonFile!)
        let jsonData = try? Data(contentsOf: jsonFileUrl)
        
        let obj = try _parse(jsonData)

        return self.decoder(obj)
    }
    
    func deserialize(json: String) throws -> T? {
        let data = json.data(using: String.Encoding.utf8)
        let obj = try _parse(data)
        
        return self.decoder(obj)
    }
}
