//
//  CountryTableViewCell.swift
//  lab2
//
//  Created by Дмитрий Мишин on 22.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    private var _country: Country?

    var country: Country? {
        set(value) {
            self._country = value
            
            name.text = value?.name
            shortName.text = value?.code
            code.text = country?.isoNumber
        }
        get {
            return self._country
        }
    }
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var shortName: UILabel!
    
    @IBOutlet weak var code: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
