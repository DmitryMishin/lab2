//
//  ProductTableViewController.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import UIKit

class ProductTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let _logger: Logger = ConsoleLogger()
    var productsModel: ProductsModel?
    var activeIndex: Int? = nil
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (_products?.count)!;
    }
    
    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?
    {
        self.activeIndex = indexPath.row
        
        return(indexPath)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: ProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductTableViewCell
        
        cell.product = self._products?[indexPath.row]
     
        return cell;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productsModel = ProductsModel(logger: self._logger)
        productsModel?.loadProducts()
    }
    
    private var _products: [Product]? {
        get {
            return productsModel?.products
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  let destinationVC = segue.destination as? DescriptionViewController,
            let index = self.activeIndex {
            
            destinationVC.product = self._products?[index]
        }
    }
}
