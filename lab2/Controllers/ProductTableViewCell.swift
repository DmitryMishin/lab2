//
//  ProductTableViewCell.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    private var _product: Product? = nil
    
    var product: Product? {
        set(value) {
            if value != nil {
                self._product = value
                self.cellTitle.text = value?.title
                self.cellImage.image = UIImage(named: (value?.photoUrl)!)
                self.cellDescription.text = value?.description
            }
        }
        get {
            return self._product
        }
    }
    
    @IBOutlet weak var cellDescription: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
