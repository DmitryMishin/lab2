//
//  CountryTableViewController.swift
//  lab2
//
//  Created by Дмитрий Мишин on 21.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import UIKit
import Alamofire

class CountryTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let _logger: Logger = ConsoleLogger()
    var countryModel: CountryModel?
    var _countries: [Country]? = [Country]()
    
    @IBOutlet weak var tableView: UITableView!
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (_countries?.count)!;
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CountryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CountryTableViewCell
        
        cell.country = self._countries?[indexPath.row]
        
        return cell;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tableView.dataSource = self
//        tableView.delegate = self
        
        countryModel = CountryModel(logger: self._logger)
        countryModel?.loadProducts(updateProducts: {
            () -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self._countries = self.countryModel?.countries
                self.tableView.reloadData()
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
