//
//  DescriptionViewController.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import UIKit

class DescriptionViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var fullDetails: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    var product: Product? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productTitle.text = product?.title
        productImage.image = UIImage(named: (product?.photoUrl)!)
        productDescription.text = product?.description
        fullDetails.text = product?.fullDetails
        
        view.autoresizingMask = .flexibleHeight
        
//        scrollView.autoresizingMask = .flexibleHeight
//        scrollView.autoresizesSubviews = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
