//
//  CountryModel.swift
//  lab2
//
//  Created by Дмитрий Мишин on 21.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation
import Alamofire

class CountryModel {
    private var _countries: [Country] = [Country]()
    private var _logger: Logger?
    
    public var countries: [Country]! {
        get {
            return self._countries
        }
        set(value) {
            self._countries = value
        }
    }
    
    init(logger: Logger) {
        self._logger = logger;
    }
    
    func loadProducts(updateProducts: @escaping (() -> Void)) {
        Alamofire.request("https://ws.postcoder.com/pcw/PCW58-C9N2Y-BM39R-56YVM/country?format=json").responseJSON { response in
            if let result = response.result.value {
                self.countries = Country.decodeProductsArray(obj: result)
                updateProducts()
            }
        }
    }
}
