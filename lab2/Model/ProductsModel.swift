//
//  ProductsModel.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class ProductsModel {
    private var _products: [Product]?
    private var _logger: Logger?
    
    public var products: [Product]! {
        get {
            return self._products
        }
    }
    
    init(logger: Logger) {
        self._logger = logger;
    }
    
    func loadProducts() {
        do {
            self._products = try JsonParser<[Product]>(decoder: Product.decodeProductsArray)
                .deserialize(path: "test", type: "json")!
        } catch {
            _logger?.error(message: "Unhandled error")
        }
    }
}
