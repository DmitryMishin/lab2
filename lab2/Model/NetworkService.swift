//
//  NetworkService.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class NetworkService
{
    lazy var configuration: URLSessionConfiguration =
        URLSessionConfiguration.default
    lazy var session: URLSession =
        URLSession(configuration: self.configuration)
    
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    typealias ImageDataHandler = ((Data) -> Void)
    
    func downloadImage(_ completion: @escaping ImageDataHandler)
    {
        let request = URLRequest(url: self.url)
        let dataTask = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            if error == nil {
                if let httpResponse = response as? HTTPURLResponse {
                    switch (httpResponse.statusCode) {
                    case 200:
                        if let data = data {
                            completion(data)
                        }
                    default:
                        print(httpResponse.statusCode)
                    }
                }
            } else {
                print("Error")
            }
        })
        
        dataTask.resume()
    }
}

extension NetworkService
{
    static func parseJsonFormat(_ jsonData: Data?) -> Any?
    {
        if let data = jsonData {
            do {
                let jsonDictinary = try JSONSerialization
                    .jsonObject(with: data, options: JSONSerialization.ReadingOptions
                        .mutableContainers)
                
                return(jsonDictinary)
            } catch _ as NSError {
                
            }
        }
        return(nil)
    }
}
