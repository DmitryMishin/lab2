//
//  Product.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class Product {
    public var title: String?
    public var description: String?
    public var photoUrl: String?
    public var fullDetails: String?
    
    init(title: String) {
        self.title = title;
    }
    
    init(dictinary: [String: Any]) {
        self.title = dictinary["title"] as? String
        self.description = dictinary["description"] as? String
        self.photoUrl = dictinary["photo_url"] as? String
        self.fullDetails = dictinary["full_details"] as? String
    }
    
    static func decodeProductsArray(obj: Any) -> [Product] {
        let epsDictinaries = obj as! [[String: Any]]
            
        var products = [Product]()
            
        for item in epsDictinaries {
            let newProduct = Product(dictinary: item)
                
            products.append(newProduct)
        }
        
        return(products)
    }
}
 
