//
//  ProductsLoader.swift
//  lab2
//
//  Created by Дмитрий Мишин on 19.02.17.
//  Copyright © 2017 Дмитрий Мишин. All rights reserved.
//

import Foundation

class ProductsLoader {
    private let path: String;
    
    init(path: String) {
        self.path = path;
    }
    
    public func readFile() {
        if let filepath = Bundle.main.path(forResource: path, ofType: "json") {
            do {
                let contents = try String(contentsOfFile: filepath)
                print(contents)
            } catch {
                // contents could not be loaded
            }
        } else {
            // example.txt not found!
        }
    }
    
    public func Load() {
        
    }
}
